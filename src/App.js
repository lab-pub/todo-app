import './App.css';
import List from './components/List/List.component';
import Add from './components/Add/Add.component';
import { useState } from 'react';

function App() {
  const [tasks, setTasks] = useState([])

  const addTask = (value) => {
    setTasks([...tasks,{id:tasks.length+1,desc:value,status:"in Progess"}]);
  }

  const finishTask = (task) => {
    const updated = tasks.map(item => { if(item.id == task.id)  item.status = "finished" ;return item })
    setTasks(updated)
  }
  return (
    <section className="vh-100" style={{ "backgroundColor": "#eee" }}>
      <div className="container py-5 h-100">
        <div className="row d-flex justify-content-center align-items-center h-100">
          <div className="col col-lg-9 col-xl-7">
            <div className="card rounded-3">
              <div className="card-body p-4">
                <h4 className="text-center my-3 pb-3">To Do App</h4>
                <Add addTask={addTask}/>
                <List tasks={tasks} handleFinish={finishTask}/>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default App;
