import React from 'react'
import './List.style.css'
export default function List({tasks,handleFinish}) {
  return (
      <React.Fragment>
              <table className="table mb-4">
                <thead>
                  <tr>
                    <th scope="col">No.</th>
                    <th scope="col">Todo item</th>
                    <th scope="col">Status</th>
                    <th scope="col">Actions</th>
                  </tr>
                </thead>
                <tbody>
                    {tasks.map(task => (
                    <tr key={task.id} className={task.status == "finished" ? 'finished' : ""}>
                    <th scope="row">{task.id}</th>
                    <td>{task.desc}</td>
                    <td>{task.status}</td>
                    <td>
                      <button type="submit" className="btn btn-danger">Delete</button>
                      <button type="submit" onClick={() => handleFinish(task)} className="btn btn-success ms-1">Finished</button>
                    </td>
                  </tr>))}
                  
                
                </tbody>
              </table>
   </React.Fragment>
  )
}
