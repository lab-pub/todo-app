import { useState } from "react"


const Add = ({addTask}) => {
    const [task, setTask] = useState("")

   
    const updateInput = (event) => {
        setTask(event.target.value);
    }
    const cleanInput = () => {
        setTask("");
 
    }
    const onClick = (task) => {
        addTask(task);
        cleanInput()
    }
    return (
        <div className="col-12">
            <div className="input-group mb-3">
                <input type="text" value={task} onChange={updateInput} id="task" className="form-control" />
                <button type="submit" onClick={() => onClick(task)} className="btn btn-primary">Save</button>
            </div>
        </div>

        )
}

export default Add;